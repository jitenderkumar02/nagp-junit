package com.Trader.eBroker.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "trader")
public class Trader {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String traderId;
	
	private String traderName;
	
	private Integer fund;

	public Trader(String traderId, String traderName, Integer fund) {
		this.traderId = traderId;
		this.traderName = traderName;
		this.fund = fund;
	}
	
	public Trader() {
		
	}

	public String getTraderId() {
		return traderId;
	}

	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public Integer getFund() {
		return fund;
	}

	public void setFund(Integer fund) {
		this.fund = fund;
	}
}
