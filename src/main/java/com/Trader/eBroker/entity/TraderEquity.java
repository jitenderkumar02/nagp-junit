package com.Trader.eBroker.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity(name = "TraderEquity")
public class TraderEquity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@OneToOne
	@JoinColumn(name = "user")
	@NotNull
	private Trader trader;

	@OneToOne
	@JoinColumn(name = "equity")
	@NotNull
	private Equity equity;

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	public Equity getEquity() {
		return equity;
	}

	public void setEquity(Equity equity) {
		this.equity = equity;
	}

	public String getId() {
		return id;
	}

}
