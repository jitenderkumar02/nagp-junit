package com.Trader.eBroker.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "equity")
public class Equity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String equityId;

	public String getEquityId() {
		return equityId;
	}

	public void setEquityId(String equityId) {
		this.equityId = equityId;
	}

	public String getEquityName() {
		return equityName;
	}

	public void setEquityName(String equityName) {
		this.equityName = equityName;
	}

	public Integer getEquityPrice() {
		return equityPrice;
	}

	public void setEquityPrice(Integer equityPrice) {
		this.equityPrice = equityPrice;
	}

	private String equityName;
	
	private Integer equityPrice;
	
	public Equity(String equityId, String equityName, Integer equityPrice) {
		super();
		this.equityId = equityId;
		this.equityName = equityName;
		this.equityPrice = equityPrice;
	}
	
	public Equity() {
		
	};
}
