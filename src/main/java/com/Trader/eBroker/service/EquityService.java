package com.Trader.eBroker.service;

import com.Trader.eBroker.entity.Equity;

public interface EquityService {
	
	Equity addEquity(Equity equity);
	
	Equity getEquity(String equityId);
}
