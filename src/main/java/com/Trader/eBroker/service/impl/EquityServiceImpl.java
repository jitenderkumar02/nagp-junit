package com.Trader.eBroker.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Trader.eBroker.entity.Equity;
import com.Trader.eBroker.repository.EquityRepository;
import com.Trader.eBroker.service.EquityService;

@Service
public class EquityServiceImpl implements EquityService {

	@Autowired
	EquityRepository repository;

	@Override
	public Equity addEquity(Equity equity) {
		Equity equityDTO = repository.save(equity);
		return equityDTO;
	}

	@Override
	public Equity getEquity(String equityId) {
		Optional<Equity> equityDTO = repository.findById(equityId);
		if(equityDTO.isPresent()) {
			return equityDTO.get();
		}
		throw new IllegalArgumentException();
	}
}
