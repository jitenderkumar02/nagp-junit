package com.Trader.eBroker.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Trader.eBroker.entity.Equity;
import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.entity.TraderEquity;
import com.Trader.eBroker.repository.EquityRepository;
import com.Trader.eBroker.repository.TraderEquityRepository;
import com.Trader.eBroker.repository.TraderRepository;
import com.Trader.eBroker.service.TraderService;
import com.Trader.eBroker.utils.TradingTimeCalculatorUtil;

@Service
public class TraderServiceImpl implements TraderService{
	
	@Autowired
	TraderRepository traderRepository;
	
	@Autowired
	EquityRepository equityRepository;
	
	@Autowired
	TraderEquityRepository traderEquityRepository;

	@Override
	public Trader addTrader(Trader trader) {
		Trader traderDTO = traderRepository.save(trader);
		return traderDTO;
	}

	@Override
	public Trader getTrader(String traderId) {
		Optional<Trader> traderDTO = traderRepository.findById(traderId);
		if(traderDTO.isPresent()) {
			return traderDTO.get();
		}
		return null;
	}
	
	@Override
	public TraderEquity buyEquity(String traderId, String equityId) {
		if(validateTradingTime() == true) {
			Trader trader = null;
			Optional<Trader> traderOpt = traderRepository.findById(traderId);
			if(traderOpt.isPresent()) {
				trader = traderOpt.get();
			}
			
			Equity equity = null;
			Optional<Equity> equityOpt = equityRepository.findById(equityId);
			if(equityOpt.isPresent()) {
				equity = equityOpt.get();
			}
			
			if(  trader != null && equity != null && trader.getFund() > equity.getEquityPrice()) {
				TraderEquity traderEquity = new TraderEquity();
				traderEquity.setEquity(equity);
				traderEquity.setTrader(trader);
				traderEquityRepository.save(traderEquity);
				
				Integer updateFund = trader.getFund() - equity.getEquityPrice();
				trader.setFund(updateFund);
				traderRepository.save(trader);
			}
		}
				
		return null;
	
	}
	
	@Override
	public TraderEquity sellEquity(String traderId, String equityId) {
		if(validateTradingTime() == true) {
			Trader trader = null;
			Optional<Trader> traderOpt = traderRepository.findById(traderId);
			if(traderOpt.isPresent()) {
				trader = traderOpt.get();
				List<TraderEquity> traderEquityList = traderEquityRepository.findAllByTrader(trader);
				
				Integer fundFromEquitySell = 0;
				for(TraderEquity traderEquity : traderEquityList) {
					Equity equity = traderEquity.getEquity();
					if(equity.getEquityId().equals(equityId)) {
						fundFromEquitySell += equity.getEquityPrice();
						traderEquityRepository.deleteById(traderEquity.getId());
					}
				}
				fundFromEquitySell = trader.getFund() + fundFromEquitySell;
				trader.setFund(fundFromEquitySell);
				traderRepository.save(trader);
				System.out.println("################" + traderEquityList);
			}	
			
		}
				
		return null;
	}
	
	
	 public Boolean validateTradingTime(){
	       return TradingTimeCalculatorUtil.checkTradingTimeValidity();
	 }
}
