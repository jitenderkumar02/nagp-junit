package com.Trader.eBroker.service;

import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.entity.TraderEquity;

public interface TraderService {
	
	Trader addTrader(Trader trader);
	
	Trader getTrader(String traderId);
	
	TraderEquity buyEquity(String traderId, String equityId);
	
	TraderEquity sellEquity(String traderId, String equityId);
}
