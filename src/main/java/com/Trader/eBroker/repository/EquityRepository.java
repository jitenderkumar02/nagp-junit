package com.Trader.eBroker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Trader.eBroker.entity.Equity;

public interface EquityRepository extends JpaRepository<Equity, String>{

}
