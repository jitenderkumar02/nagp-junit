package com.Trader.eBroker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.entity.TraderEquity;

public interface TraderEquityRepository extends JpaRepository<TraderEquity, String> {

	public List<TraderEquity> findAllByTrader(Trader Trader);
}
