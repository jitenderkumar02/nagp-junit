package com.Trader.eBroker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Trader.eBroker.entity.Trader;

public interface TraderRepository extends JpaRepository<Trader, String>{

}
