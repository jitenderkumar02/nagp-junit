package com.Trader.eBroker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Trader.eBroker.entity.Equity;
import com.Trader.eBroker.service.EquityService;

@RestController
public class EquityController {

	@Autowired
	EquityService service;
	
	@GetMapping(path = "getEquity")
	ResponseEntity<Object> getEquity(@RequestParam String equityId) {
		Equity equity = service.getEquity(equityId);
		return  ResponseEntity.ok().body(equity);
	}
	
	@PostMapping(path = "addEquity" , consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Object> addTrader(@RequestBody Equity equity) {
		System.out.println("jitender");
		equity = service.addEquity(equity);
		return ResponseEntity.ok().body(equity);
	}
}
