package com.Trader.eBroker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.entity.TraderEquity;
import com.Trader.eBroker.service.TraderService;


@RestController
public class TraderController {

	@Autowired
	TraderService traderService;
	
	@GetMapping(path = "getTrader")
	Object getTrader(@RequestParam String traderId) {
		Trader traderDTO = traderService.getTrader(traderId);
		return traderDTO;
	}
	
	@PostMapping(path = "addFund")
	Object addFund(@RequestParam String traderId , @RequestParam Integer fund ) {
		Trader trader = traderService.getTrader(traderId);
		if(trader != null) {
			trader.setFund(trader.getFund() +  fund);
			Trader traderDTO = traderService.addTrader(trader);
			return traderDTO;
		}
		return "Trader with Id " + traderId + " not found";
	}
	
	@PostMapping(path = "addTrader", consumes = MediaType.APPLICATION_JSON_VALUE)
	Object addTrader(@RequestBody Trader trader) {
		Trader traderDTO = traderService.addTrader(trader);
		return traderDTO;
	}
	
	@PostMapping(path = "buyEquity")
	Object buyEquity(@RequestParam String traderId , @RequestParam String equityId ) {
		TraderEquity traderEquity = traderService.buyEquity(traderId, equityId);
		return traderEquity;
	}
	
	@PostMapping(path = "sellEquity")
	Object sellEquity(@RequestParam String traderId , @RequestParam String equityId ) {
		TraderEquity traderEquity = traderService.sellEquity(traderId, equityId);
		return traderEquity;
	}
}
