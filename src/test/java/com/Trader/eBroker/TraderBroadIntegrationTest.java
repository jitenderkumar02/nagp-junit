package com.Trader.eBroker;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class TraderBroadIntegrationTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testAddTraderGetTraderAddFundBuyEquity() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/addTrader")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"traderId\": \"\", \"traderName\":\"Jitender\", \"fund\":5000}"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(5000)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
				.param("traderId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(5000)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/addFund")
				.param("traderId", "1")
				.param("fund", "2000"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(7000)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/addFund")
				.param("traderId", "10")
				.param("fund", "2000"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Trader with Id 10 not found")));
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
				.param("traderId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(7000)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/addEquity")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"equityId\": \"\", \"equityName\":\"HDFC\", \"equityPrice\":250}"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityPrice", Matchers.is(250)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityName", Matchers.is("HDFC")));
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getEquity")
				.param("equityId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityPrice", Matchers.is(250)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityName", Matchers.is("HDFC")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/buyEquity")
				.param("traderId", "1")
				.param("equityId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
				.param("traderId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(6750)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/buyEquity")
				.param("traderId", "1")
				.param("equityId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
				.param("traderId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(6500)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));
		
		mockMvc.perform(MockMvcRequestBuilders.post("/sellEquity")
				.param("traderId", "1")
				.param("equityId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
				.param("traderId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(7000)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")));		
		
	}
}
