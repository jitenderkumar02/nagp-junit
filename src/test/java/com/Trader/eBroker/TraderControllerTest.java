package com.Trader.eBroker;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.Trader.eBroker.controller.TraderController;
import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.service.impl.TraderServiceImpl;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TraderController.class)
public class TraderControllerTest {

	@MockBean
	private TraderServiceImpl traderService;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testGetTrader() throws Exception{
		Trader trader = new Trader("1", "Jitender", 7000);
		Mockito.when(traderService.getTrader("1")).thenReturn(trader);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getTrader")
		.param("traderId", "1"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$.traderName", Matchers.is("Jitender")))
		.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(7000)));
		
	}
	
	@Test
	public void testAddTrader() throws Exception{
		
		Trader trader = new Trader("", "Jitender", 7000);
		Trader traderReturned = new Trader("1", "Jitender", 7000);
		Mockito.when(traderService.addTrader(trader)).thenReturn(traderReturned);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/addTrader")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"traderId\": \"\", \"traderName\":\"jitnder\", \"fund\":5000}"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print());
		
	}
}
