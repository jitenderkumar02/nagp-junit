package com.Trader.eBroker;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class EquityBroadIntegrationTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testAddGet() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/addEquity")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"equityId\": \"\", \"equityName\":\"HDFC\", \"equityPrice\":250}"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityPrice", Matchers.is(250)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityName", Matchers.is("HDFC")));
		
		mockMvc.perform(MockMvcRequestBuilders.get("/getEquity")
				.param("equityId", "1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityPrice", Matchers.is(250)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.equityName", Matchers.is("HDFC")));
		
	}
}
