package com.Trader.eBroker;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.Trader.eBroker.entity.Equity;
import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.repository.EquityRepository;
import com.Trader.eBroker.repository.TraderRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DaoTest {

	@Autowired
	private TraderRepository traderRepository;
	
	@Autowired
	private EquityRepository equityRepository;
	
	@Test
	public void testAddGetTrader(){
		
		Trader trader = new Trader("", "Jitender", 7000);
		traderRepository.save(trader);
		
		Trader traderDB = traderRepository.getById("1");
		
		Assertions.assertEquals(trader.getFund(), traderDB.getFund());
		Assertions.assertEquals(trader.getTraderName(), traderDB.getTraderName());
		
		traderRepository.deleteById("1");
		
		List<Trader> traderList = traderRepository.findAll();
		
		Assertions.assertEquals(traderList.size(), 0);
	}
	
	@Test
	public void testAddGetEquity(){
		
		Equity equity = new Equity("", "HDFC", 250);
		equityRepository.save(equity);
		
		Equity equityDB = equityRepository.getById("1");
		
		Assertions.assertEquals(equity.getEquityName(), equityDB.getEquityName());
		Assertions.assertEquals(equity.getEquityPrice(), equityDB.getEquityPrice());
		
		equityRepository.deleteById("1");
		
		List<Equity> equityList = equityRepository.findAll();
		
		Assertions.assertEquals(equityList.size(), 0);
		
	}
}
