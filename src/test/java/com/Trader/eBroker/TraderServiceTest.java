package com.Trader.eBroker;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import com.Trader.eBroker.entity.Trader;
import com.Trader.eBroker.repository.TraderRepository;
import com.Trader.eBroker.service.TraderService;
import com.Trader.eBroker.service.impl.TraderServiceImpl;
import com.Trader.eBroker.utils.TradingTimeCalculatorUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.mockito.MockedStatic;

import java.util.Optional;

public class TraderServiceTest {

	@InjectMocks
    private TraderServiceImpl traderService;
	
	@Mock
	private TraderRepository traderRepository;
	
	@BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }
	
	@DisplayName("Test getting Trader with Trader Id  with Repository.")
    @Test
    public void tesGetingTraderWithRepository(){
 
        Trader traderExpected = new Trader("1", "Jitender", 7000); 
		
        Mockito.when(traderRepository.findById("1")).thenReturn(Optional.of(traderExpected));

        Trader getTraderActual = traderService.getTrader("1"); 

        Assertions.assertEquals(traderExpected, getTraderActual);
    }
	
	
	@DisplayName("Test Adding Trader with Repository.")
    @Test
    public void testAddTraderWithRepository(){

        Trader trader = new Trader("", "Jitender Kumar", 7000); 
        Trader traderExpected = new Trader("1", "Jitender", 7000); 

        Mockito.when(traderRepository.save(trader)).thenReturn(traderExpected);

        Trader traderActual = traderService.addTrader(trader); 

        Assertions.assertEquals(traderExpected, traderActual);
    }
	
	@DisplayName("Example Mockito static method mocking")
    @Test
    public void givenStaticMethodWithNoArgsWhenMockedThenItWillReturn() {

        Assertions.assertEquals(true, TradingTimeCalculatorUtil.checkTradingTimeValidity());

        try (MockedStatic<TradingTimeCalculatorUtil> utilites = Mockito.mockStatic(TradingTimeCalculatorUtil.class)) {
            utilites.when(TradingTimeCalculatorUtil::checkTradingTimeValidity).thenReturn(false);
            Assertions.assertEquals(false, TradingTimeCalculatorUtil.checkTradingTimeValidity());
        }
        Assertions.assertEquals(true, TradingTimeCalculatorUtil.checkTradingTimeValidity());
    }

}
