package com.Trader.eBroker;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.Trader.eBroker.entity.Equity;
import com.Trader.eBroker.repository.EquityRepository;
import com.Trader.eBroker.service.impl.EquityServiceImpl;

public class EquityServiceTest {

	@InjectMocks
	EquityServiceImpl equityService;
	
	@Mock
	EquityServiceImpl equityServiceMock;
	
	@Mock
	EquityRepository equityRepository;
	
	@BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }
	
	
	@DisplayName("Test getting Equity with Equity Id  with Repository.")
    @Test
    public void tesGetingEquityWithRepository(){
 
		Equity equityExpected = new Equity("1", "LDFC", 250); 
		
        Mockito.when(equityRepository.findById("1")).thenReturn(Optional.of(equityExpected));

        Equity getEquityActual = equityService.getEquity("1"); 

        Assertions.assertEquals(equityExpected, getEquityActual);
    }
	
	@DisplayName("Test Adding Equity with Repository.")
    @Test
    public void testAddEquityWithRepository(){

		Equity equity = new Equity("", "HDFC", 250); 
		
		Equity equityExpected = new Equity("1", "HDFC", 250); 

        Mockito.when(equityRepository.save(equity)).thenReturn(equityExpected);

        Equity equityActual = equityService.addEquity(equity); 

        Assertions.assertEquals(equityExpected, equityActual);
    }
	
	@DisplayName("Test getting Equity with Equity Id Throw Exception with Repository.")
    @Test
    public void tesGetingEquityThrowExceptionWithRepository(){
		 		
		Mockito.doThrow(RuntimeException.class).when(equityServiceMock).getEquity("10");

		
		Assertions.assertThrows(RuntimeException.class,
	                () -> equityService.getEquity("10"));
    }
}
